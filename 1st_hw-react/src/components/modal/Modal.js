import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Modal.scss';
class Modal extends Component{

  render(){
    const {header, onCancel, onSubmit, text, bcgColor} = this.props;

  
    return (<>
            <div className='Modal' onClick={onCancel}>
            </div>
            <div className='openModal'>
            <div className='headerOfModal'>{header}<div className='close' onClick={onCancel}></div></div>
            <div className='bodyOfModal'>{text}</div>
            <div className='buttons'>
                <button className='buttonOfModal OK' onClick={onSubmit} style={{backgroundColor: bcgColor}}>Ok</button>
                <button className='buttonOfModal Cancel' onClick={onCancel} style={{backgroundColor: bcgColor}}>Cancel</button>
            </div>
          </div>
          </>
            
    );
  }
  
  };


Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  bcgColor: PropTypes.string,
};

Modal.defaultProps = {
  header: 'Modal title',
  text: 'test',
  onCancel: () => {},
  onSubmit: () => {},
  bcgColor: '#b3382c'
};

export default Modal;