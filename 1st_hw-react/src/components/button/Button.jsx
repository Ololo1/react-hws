import React, {Component} from 'react';
import './button.scss';
import propTypes from 'prop-types';

class Button extends Component {
    render() {
      
      const {bcgColor,text, handleClick} = this.props;
      
        return ( 
          <>
                <button onClick={handleClick} className='btn' style={{backgroundColor:bcgColor}}>{text}</button> 
          </>)
    }
  }


Button.propTypes = {
  text: propTypes.string,
  onClick: propTypes.func,
  className: propTypes.string,
};

Button.defaultProps = {
  text: 'Button!',
  onClick: ()=> {},
  className: 'btn',
};

export default Button;
