import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './theme/styles/index.scss';
// import ButtonSandbox from './components/button/Sandbox';
// import registerServiceWorker from './registerServiceWorker';
// import { BrowserRouter, Switch, Route } from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
        <App/>
    </BrowserRouter>, document.getElementById('root'));


serviceWorker.unregister();

 