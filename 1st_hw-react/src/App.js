import React, {Component} from 'react';
import './App.css';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';

class App extends Component{
  state = {
    isModal1Open: false,
    isModal2Open: false,
  };

  toggleModal1 = () => {
    this.setState((state) => {
      return {
        isModal1Open: !state.isModal1Open,
      }
    });
  };

  toggleModal2 = () => {
    this.setState((state) => {
      return {
        isModal2Open: !state.isModal2Open,
      }
    });
  };

  onCancel = () => {
    console.log('Cancel function!');
      this.setState({ isModal1Open: false , isModal2Open: false});
  }

  onSubmit=() => {
    console.log('Submit function!');
        this.setState({ isModal1Open: false ,
          isModal2Open: false
        });
  }

  render(){
    return(
      <div className="App">
        <div className='button'>
          <Button
            handleClick={this.toggleModal1}
            bcgColor= 'rgb(49, 218, 72)'
            text= 'Open first modal'
          /> 
           <Button 
            handleClick={this.toggleModal2}
            bcgColor= '#3b13a8'
            text= 'Open second modal'
          /> 
        </div>

        {this.state.isModal1Open && <Modal  header = 'Do you want to delete this file?'
            text = 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
            onCancel={this.onCancel}
            onSubmit={this.onSubmit}
            />}

        {this.state.isModal2Open && <Modal  header = 'Do you want to improve your knowledge?'
            text = 'Just press "OK"'
            onCancel={this.onCancel}
            onSubmit={this.onSubmit}
            bcgColor='#f7760c'/>}
        
      </div>
    )
  }
}

export default App;


